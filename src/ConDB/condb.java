package ConDB;

import com.mysql.jdbc.StatementImpl;
import java.sql.*;

public class condb {
    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/catservletdata?useSSL=false&serverTimezone=UTC&characterEncoding=utf-8&allowPublicKeyRetrieval=true&user=root&password=yuxl19990515");
            System.out.println("connect success!");
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("connect faild!");
        return null;
    }
    //select
    public static ResultSet executeQuery(String SQL)
    {
        try
        {
            Connection conn=getConnection();
            Statement stmt=conn.createStatement();
            ResultSet rs=stmt.executeQuery(SQL);
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("check faild!");
            return null;
        }
    }
    //insert update delete
    public static boolean executeUpdate(String SQL)
    {
        try
        {
            Connection conn=getConnection();
            Statement stmt=conn.createStatement();
            int rs=stmt.executeUpdate(SQL);
            if (rs>0)
                return true;
            else
                return false;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("updata faild!");
            return false;
        }
    }
    public static void ShutDown(StatementImpl statement, Connection connection) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
}