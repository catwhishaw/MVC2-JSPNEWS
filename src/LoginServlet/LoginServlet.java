package LoginServlet;

import DBdata.Users;
import Service.UsersService;
import Util.CookiesSave;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author CatWhishaw
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=utf-8");
        request.setCharacterEncoding("utf-8");
        //Login的处理
        try {
            Login(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        void Login(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
            Users user=new Users();
            user.setName(request.getParameter("username"));
            user.setPassword(request.getParameter("password"));
            System.out.println("name:"+user.getName());
            System.out.println("password:"+user.getPassword());
            UsersService usersService=new UsersService();
            boolean hasUsers=usersService.QueryUsers(user);
            if(hasUsers){
                CookiesSave cookieSave=new CookiesSave();
                cookieSave.Save(response,"username",user.getName(),60*60*24*30);
                cookieSave.Save(response,"password",user.getPassword(),60*60*24*30);
                response.sendRedirect("background/index.html");
            }else
            {
                response.getWriter().println("Login faild!");
                response.sendRedirect("login.html");
            }
        }
}
