<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>产品管理</title>
<link rel="stylesheet" type="text/css" href="css/Iframe.css" />
<link rel="stylesheet" href="utilLib/bootstrap.min.css" type="text/css" media="screen" />
</head>

<body>
<span class="cp_title">新闻管理</span>
<div class="add_cp">
    <a href="AddNewServlet">+添加新闻</a>
</div>
<div class="table_con ">
	<table class="table-hover">
    	<tr class="tb_title">
        	<td width="5%" style="overflow-x: hidden">ID</td>
            <td width="25%" style="overflow-x: hidden">标题</td>
            <td width="20%" style="overflow-x: hidden">内容</td>
            <td width="10%" style="overflow-x: hidden">作者</td>
            <td width="10%" style="overflow-x: hidden">类别</td>
            <td width="10%" style="overflow-x: hidden">时间</td>
            <td width="20%" style="overflow-x: hidden">操作</td>
        </tr>
            <c:forEach var="n" items="${lsNews}" >
            <tr>
            <td width="5%"  style="overflow-x: hidden">${n.idnews}</td>
            <td width="25%" style="overflow-x: hidden">${n.title}</td>
            <td width="20%" style="overflow-x: hidden">${n.content} </td>
            <td width="10%" style="overflow-x: hidden">${n.author}</td>
            <td width="10%" style="overflow-x: hidden">${n.category}</td>
            <td width="10%" style="overflow-x: hidden">${n.newsdate}</td>
            <td width="20%">
                <a href="EditNewServlet?idnews=${n.idnews}" class="btn btn-success">编辑</a>
                <a href="ViewNewServlet?idnews=${n.idnews}" class="btn btn-primary">查看</a>
                <a href="DeleteNewServlet?idnews=${n.idnews}" class="btn btn-danger">删除</a>
            </td>
            </tr>
            </c:forEach>
    </table>
</div>
</body>
</html>
