<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0028)https://www.swpu.edu.cn/scs/ -->
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>西南石油大学 - 计算机科学学院</title>
    <meta name="keywords" content="西南石油大学,计算机科学学院">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" type="text/css" href="./SWPUnews/style.css">
    <link rel="stylesheet" type="text/css" href="./SWPUnews/publice.css">
    <script type="text/javascript" src="./SWPUnews/jquery.min.js"></script>
    <script type="text/javascript" src="./SWPUnews/superslide.js"></script>
    <script type="text/javascript" src="./SWPUnews/index.js"></script>
    <script type="text/javascript" src="./SWPUnews/jquery.soChange.js"></script>
    <script type="text/javascript">
        $(function() {
            $(".mainmenu dl").hide();
            $("li.mainmenu").hover(function() {
                $(this).find("dl").stop(true, true);
                $(this).find("dl").slideDown();
            }, function() {
                $(this).find("dl").stop(true, true);
                $(this).find("dl").slideUp();
            });
        })
    </script>
    <!--Announced by Visual SiteBuilder 9-->
    <link rel="stylesheet" type="text/css" href="./SWPUnews/_sitegray_d.css">
    <script language="javascript" src="./SWPUnews/_sitegray.js"></script>
    <!-- CustomerNO:7765626265723230776447565352574103080004 -->
    <link rel="stylesheet" type="text/css" href="./SWPUnews/index.vsb.css">
    <script type="text/javascript" src="./SWPUnews/vsbscreen.min.js" id="_vsbscreen" devices="pc|mobile|pad"></script>
    <script type="text/javascript" src="./SWPUnews/counter.js"></script>
    <script type="text/javascript">
        _jsq_(1033, '/EditNews.jsp', -1, 1459835785)
    </script>
</head>

<body style="position: relative">
    <!--top s-->
    <a href="http://webscan.360.cn/index/checkwebsite/url/scss.bupt.edu.cn" name="d1aa96141abaa877e8ad955b964f8fa6"></a>
    <div class="topWrap clearfix">
        <div style="width:100%;height:112px;background:url(img/top-bg.jpg) no-repeat center top">
            <div class="topDiv">
                <div class="topR fr">
                    <script language="javascript" src="./SWPUnews/dynclicks.js"></script>
                    <script language="javascript" src="./SWPUnews/openlink.js"></script>
                    <div class="topR_t fr" style="color:#fff;">
                    </div>
                    <script type="text/javascript">
                        function _nl_ys_check_2() {
                            var keyword = document.getElementById('showkeycode192730_2').value;
                            if (keyword == null || keyword == "") {
                                alert("请输入关键字搜索");
                                return false;
                            }
                            if (window.toFF == 1) {
                                document.getElementById("lucenenewssearchkey192730").value = Simplized(keyword);
                            } else {
                                document.getElementById("lucenenewssearchkey192730").value = keyword;
                            }
                            var base64 = new Base64();
                            document.getElementById("lucenenewssearchkey192730").value = base64.encode(document.getElementById("lucenenewssearchkey192730").value);
                            new VsbFormFunc().disableAutoEnable(document.getElementById("showkeycode192730_2"));
                            return true;
                        }

                        function submitForm() {
                            document.getElementById("au1a").submit();
                        }
                    </script>
                    <script type="text/javascript">
                        function test3() {
                            var tempStr = document.getElementById('showkeycode192730_2').value;
                            if (tempStr == "请输入关键字搜索") {
                                document.getElementById('showkeycode192730_2').value = "";
                            }
                        }

                        function test4() {
                            var tempStr = document.getElementById('showkeycode192730_2').value;
                            if (tempStr == "") {
                                document.getElementById('showkeycode192730_2').value = "请输入关键字搜索";
                            }
                        }
                    </script>
                    <div class="Search">
                        <form action="https://www.swpu.edu.cn/scs/ssjg.jsp?wbtreeid=1033" method="post" id="au1a" name="au1a" onsubmit="return _nl_ys_check_2()">
                            <input type="hidden" id="lucenenewssearchkey192730" name="lucenenewssearchkey" value=""><input type="hidden" id="_lucenesearchtype192730" name="_lucenesearchtype" value="1"><input type="hidden" id="searchScope192730" name="searchScope"
                                value="0">
                            <input type="text" value="请输入关键字搜索" name="showkeycode" class="search_text" onfocus="test3()" onblur="test4()" id="showkeycode192730_2">
                            <div class="fdj fr"> <input type="submit" class="button pngFix" value="" style="cursor: hand"></div>
                        </form>
                    </div>
                    <script language="javascript" src="./SWPUnews/base64.js"></script>
                    <script language="javascript" src="./SWPUnews/formfunc.js"></script>
                </div>
            </div>
        </div>
    </div>
    <!--nav-->
    <div class="navWrap  clearfix" style="width: 974px">
        <div class="nav">
            <ul>
                <li>
                    <a class="link" href="https://www.swpu.edu.cn/scs/index.htm">网站首页</a>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/xygk/xyjj.htm">学院概况</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/xygk/xyjj.htm">学院简介</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xygk/xyld.htm">学院领导</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xygk/zzjg.htm">组织机构</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/bksjy/zysz.htm">本科生教育</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/bksjy/zysz.htm">专业设置</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/bksjy/dwrchzpyxm.htm">对外人才合作培养项目</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/bksjy/ksxx.htm">考试信息</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/bksjy/xkzxxx.htm">选课重修信息</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/bksjy/tzgg.htm">通知公告</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/bksjy/zlxz.htm">资料下载</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/bksjy/gxkjssp.htm">公选课教师视频</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/dpzw.jsp?urltype=tree.TreeTempUrl&amp;wbtreeid=1235">教学项目</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/dpzw.jsp?urltype=tree.TreeTempUrl&amp;wbtreeid=1244">教学获奖</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/list.jsp?urltype=tree.TreeTempUrl&amp;wbtreeid=1274">教学大纲</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/yjsjy/zsjz.htm">研究生教育</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/zsjz.htm">招生简章</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/jsjkxyjsyjxk.htm">计算机科学与技术一级学科</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/rjgcyjxk.htm">软件工程一级学科</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/wlkjaqyjxk.htm">网络空间安全一级学科</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/yjsds.htm">研究生导师</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/yjsjztx.htm">研究生奖助体系</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/tzgg.htm">通知公告</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/yjsjy/zlxz.htm">资料下载</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/kxyj/kytd.htm">科学研究</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/kxyj/kytd.htm">科研团队</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/kxyj/kypt.htm">科研平台</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/kxyj/kycg.htm">科研成果</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/xsgz/gzdt.htm">学生工作</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/xsgz/gzdt.htm">工作动态</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xsgz/tzgg.htm">通知公告</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xsgz/kwcxsj.htm">课外创新实践</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xsgz/bysjy.htm">毕业生就业</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xsgz/xlzc.htm">心灵之窗</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xsgz/qcfc.htm">青春风采</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/xsgz/zlxz.htm">资料下载</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/info/1189/4517.htm">招生工作</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/zsgz/xyjs.htm">学院介绍</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/zsgz/bysjyqx.htm">毕业生就业去向</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/zsgz/yxbysjj.htm">优秀毕业生简介</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/zsgz/jshj.htm">教师获奖</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/zsgz/xshj.htm">学生获奖</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/zsgz/jyxyjs.htm">精英校友介绍</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/zsgz/zsgzxcbd.htm">招生工作宣传报道</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/info/1183/2625.htm">实验中心</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/syzx/zxjj.htm">中心简介</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/syzx/syfs.htm">实验分室</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/syzx/gzzd.htm">规章制度</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/syzx/zlxz.htm">资料下载</a></dd>
                        <dd><a href="http://syskf.swpu.edu.cn/">开放预约</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/djzc/djdt.htm">党建之窗</a>
                    <!--如果是导航字数很多，则加上 class="chang"，否则去掉即可-->
                    <dl style="display: none;">
                        <dd><a href="https://www.swpu.edu.cn/scs/djzc/djdt.htm">党建动态</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/djzc/xxyd.htm">学习园地</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/djzc/dwzwgk.htm">党务政务公开</a></dd>
                        <dd><a href="https://www.swpu.edu.cn/scs/djzc/zlxz.htm">资料下载</a></dd>
                    </dl>
                </li>
                <li class="mainmenu">
                    <a class="link" href="https://www.swpu.edu.cn/scs/kjyq.htm" style="font-weight:bold;font-style:italic;color:#FA6A7D">抗击疫情</a>
                </li>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
        $(".mainmenu dl").hide();
    </script>
    <!--banner-->
    <div class="vsb-box">
        <div class="vsb-container a" containerid="1" columns="6">
            <div class="vsb-space bannerWrap clearfix" columns="6">
                <!--banner1-->
                <!--banner1-->
                <div class="banner_one">
                    <div id="slideBox" class="slideBox">
                        <div class="hd">
                            <ul>
                                <li class="on"></li>
                                <li></li>
                                <li></li>

                            </ul>
                        </div>
                        <div class="bd">
                            <ul>
                                <li><img src="./SWPUnews/colonavirus.jpg" style="width: 100%; height:200px"></li>
                                <li style="display: none;">
                                    <a href="https://www.swpu.edu.cn/scs/info/1033/4634.htm" target="_blank"><img src="./SWPUnews/welcome.jpg"></a>
                                </li>
                                <li style="display: none;"><img src="./SWPUnews/main-zs.jpg" style="width: 100%; height:200px"></li>
                            </ul>
                        </div>
                        <a class="prev" href="javascript:void(0)"></a>
                        <a class="next" href="javascript:void(0)"></a>

                    </div>

                </div>
                <script type="text/javascript">
                    jQuery(".slideBox").slide({
                        mainCell: ".bd ul",
                        autoPlay: false
                    });
                </script>
            </div>
        </div>
        <!--main-->
        <div class="vsb-container a" containerid="1" columns="6">
            <!--banner2-->
        </div>
        <div class="vsb-container a" containerid="1" columns="6">
            <!--banner3-->
        </div>
    </div>
    <div></div>
    <!--container-->
    <div class="vsb-box">
        <div class="vsb-container container clearfix" containerid="2" columns="6">
            <!--新闻信息-->
            <div class="vsb-space new_inforBox new_inforBox2 fl" columns="4">
                <div class="dynamic">
                    <h2>图片新闻</h2><span><a href="https://www.swpu.edu.cn/scs/index/xwsd.htm"><img src="./SWPUnews/more.png"></a></span>
                </div>
                <div class="newBox">
                    <!--新闻信息-->
                    <div class="new_pic new_pic1">
                        <link href="./SWPUnews/imagechangenews.css" type="text/css" rel="stylesheet">
                        <script language="javascript" src="./SWPUnews/imagechangenews.js"></script>
                        <script language="JavaScript">
                            var u_u4_icn = new ImageChangeNews("u_u4_", 310, 174, 4, 2.0, true, false, false, true);

                            //初始化图片, 并启动定时
                            function u_u4_init_img() {
                                u_u4_icn.addimg("./SWPUnews/82B764B3437566D7E324B94C868_3465389D_13EE6.jpg", "#", "邹正伟老师获得“2019年度四川省优秀物联网教师”...", "");
                                u_u4_icn.addimg("./SWPUnews/BBCDC8B3D65530143C22D511A64_D94DB94B_55CA.png", "#", "“翼灵杯”第七届物联网创意大赛成功举办", "");
                                u_u4_icn.addimg("./SWPUnews/03A29A3878264CE62594C1BAE99_0D4A23D3_C623.jpg", "#", "我院实验中心加入中国计算机实践教育联合会", "");
                                u_u4_icn.addimg("./SWPUnews/2D47955E866279F1690EFB6A716_109706CE_F84D.png", "#", "我院教师参加2019年大数据实战研习", "");
                                u_u4_icn.addimg("./SWPUnews/B58B7C9873821FA8404596B9AF7_CF231185_CF81.png", "#", "计科院成功举办2019年“优秀大学生暑期夏令营”", "");
                                u_u4_icn.addimg("./SWPUnews/F57F6BBBACF42D86824334B6BB1_4DE418D7_10F3B.jpg", "#", "计算机科学学院隆重举行第八届“盛特杯”大学生课...", "");
                                u_u4_icn.changeimg(0);
                            }
                        </script>
                        <div display="none" style="overflow:hidden;text-overflow:ellipsis;height:0px;width:310px;padding-left:2px;padding-right:2px">
                            <a target="_blank" class="summarystyle192939" id="u_u4_newssummary"></a>
                        </div>
                        <table cellspacing="0" cellpadding="0" style="padding:0px;margin:0px; border:0px">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="OVERFLOW: hidden;height:174px;width:310px">
                                            <div id="u_u4_div" style="padding:0px;margin:0px;border:0px solid black;background-color:#000000;height:174px;width:310px">
                                                <div id="u_u4_imgdiv" style="padding:0px;border:0px;">
                                                    <a id="u_u4_url" target="_blank" href="#"><img id="u_u4_pic" width="310" height="174" src="./SWPUnews/82B764B3437566D7E324B94C868_3465389D_13EE6.jpg"></a>
                                                </div>
                                            </div>
                                            <div style="filter:alpha(style=1,opacity=10,finishOpacity=80);width:310px;height:19px;text-align:right;top:-19px;position:relative;padding:0px;margin:0px;border:0px;">
                                                <a href="javascript:u_u4_icn.changeimg(0)" ;="" id="u_u4_selectNode0" class="imagechangenews_fnode" target="_self">1</a>
                                                <a href="javascript:u_u4_icn.changeimg(1)" ;="" id="u_u4_selectNode1" class="imagechangenews_pnode" target="_self">2</a>
                                                <a href="javascript:u_u4_icn.changeimg(2)" ;="" id="u_u4_selectNode2" class="imagechangenews_pnode" target="_self">3</a>
                                                <a href="javascript:u_u4_icn.changeimg(3)" ;="" id="u_u4_selectNode3" class="imagechangenews_pnode" target="_self">4</a>
                                                <a href="javascript:u_u4_icn.changeimg(4)" ;="" id="u_u4_selectNode4" class="imagechangenews_pnode" target="_self">5</a>
                                                <a href="javascript:u_u4_icn.changeimg(5)" ;="" id="u_u4_selectNode5" class="imagechangenews_pnode" target="_self">6</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="25">
                                        <div style="overflow:hidden;text-overflow:ellipsis;height:25px;width:310px;"><a target="_blank" class="titlestyle192939" id="u_u4_newstitle" href="https://www.swpu.edu.cn/scs/info/1045/5545.htm" title="邹正伟老师获得“2019年度四川省优秀物联网教师”...">邹正伟老师获得“2019年度四川省优秀物联网教师”...</a></div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <script language="JavaScript">
                            u_u4_init_img();
                        </script>
                    </div>
                    <div class="new_list new_list3">
                        <ul class="dynamic_list fr">
                            <c:forEach var="n" items="${lsNews_by_0}" >
                            <li><a href="ViewNewServlet?idnews=${n.idnews}" title=${n.title}><em style="width:70%">${n.title}</em><span style="width:30%">${n.newsdate}</span></a></li>
                            </c:forEach>
                        </ul>
                        <script>
                            _showDynClickBatch(['dynclicks_u5_5834', 'dynclicks_u5_5933', 'dynclicks_u5_5929', 'dynclicks_u5_5860', 'dynclicks_u5_5856', 'dynclicks_u5_5825'], [5834, 5933, 5929, 5860, 5856, 5825], "wbnews", 1459835785)
                        </script>
                    </div>
                </div>
            </div>
            <!--通知公告-->
            <div class="vsb-space informBox informBox3 fl" columns="2">
                <div class="dynamic cleafix">
                    <h2>学术交流</h2><span><a href="https://www.swpu.edu.cn/scs/kxyj/kytd.htm"><img src="./SWPUnews/more.png"></a></span></div>
                <div class="infor_list">
                    <ul class="dynamic_list dynamic_list1" style="padding-top:10px;">
                        <c:forEach var="n" items="${lsNews_by_1}" >
                        <li> <a href="ViewNewServlet?idnews=${n.idnews}" title=${n.title}>${n.title}</a></li>
                        </c:forEach>
                    </ul>
                </div>
                <script>
                    _showDynClickBatch(['dynclicks_u6_5555', 'dynclicks_u6_5554', 'dynclicks_u6_5543', 'dynclicks_u6_5403', 'dynclicks_u6_5363', 'dynclicks_u6_5354'], [5555, 5554, 5543, 5403, 5363, 5354], "wbnews", 1459835785)
                </script>
            </div>
        </div>
        <div class="vsb-container container clearfix" containerid="2" columns="6">
            <!--精品课程-->
        </div>
        <!--新闻信息-->
        <div class="vsb-container container clearfix" containerid="2" columns="6">
            <!--近期学术讲座-->
        </div>
        <div class="vsb-container container clearfix" containerid="2" columns="6">
            <!--新闻信息4-->
            <div class="vsb-space new_inforBox new_inforBoxa new_inforBoxa1 fl" columns="4">
                <div class="dynamic dynamicf">
                    <h2>新闻速递</h2><span><a href="https://www.swpu.edu.cn/scs/index/xwsd.htm"><img src="./SWPUnews/more.png"></a></span></div>
                <div class="newBox newBoxe">
                    <div class="new_list new_listd">
                        <ul class="dynamic_list dynamic_listh">
                            <c:forEach var="n" items="${lsNews_by_2}" >
                                <li><a href="ViewNewServlet?idnews=${n.idnews}" title=${n.title}><em style="width:70%">${n.title}</em><span style="width:30%">${n.newsdate}</span></a></li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
                <script>
                    _showDynClickBatch(['dynclicks_u7_5834', 'dynclicks_u7_5933', 'dynclicks_u7_5929', 'dynclicks_u7_5860', 'dynclicks_u7_5856', 'dynclicks_u7_5825', 'dynclicks_u7_5823'], [5834, 5933, 5929, 5860, 5856, 5825, 5823], "wbnews", 1459835785)
                </script>
            </div>
            <!--中国银行-->
            <div class="vsb-space informBox informBox3 fl" columns="2">
                <div class="dynamic cleafix">
                    <h2>党建动态</h2><span><a href="https://www.swpu.edu.cn/scs/djzc/djdt.htm"><img src="./SWPUnews/more.png"></a></span></div>
                <div class="infor_list">
                    <ul class="dynamic_list dynamic_list1" style="padding-top:10px;">
                        <c:forEach var="n" items="${lsNews_by_3}" >
                            <li> <a href="ViewNewServlet?idnews=${n.idnews}" title=${n.title}>${n.title}</a></li>
                        </c:forEach>
                    </ul>
                </div>
                <script>
                    _showDynClickBatch(['dynclicks_u8_5300', 'dynclicks_u8_5857', 'dynclicks_u8_5542', 'dynclicks_u8_5420', 'dynclicks_u8_5426', 'dynclicks_u8_5413', 'dynclicks_u8_5323', 'dynclicks_u8_5321'], [5300, 5857, 5542, 5420, 5426, 5413, 5323, 5321], "wbnews", 1459835785)
                </script>
            </div>
        </div>
        <!--广告-->
        <div class="vsb-container container clearfix" containerid="2" columns="6">
            <!--新闻信息4-->
            <div class="vsb-space new_inforBox new_inforBoxa new_inforBoxa1 fl" columns="4">
                <div class="dynamic cleafix">
                    <h2>通知公告</h2><span><a href="https://www.swpu.edu.cn/scs/index/tzgg.htm"><img src="./SWPUnews/more.png"></a></span></div>
                <div class="infor_list">
                    <ul class="dynamic_list dynamic_list1">
                        <c:forEach var="n" items="${lsNews_by_4}" >
                            <li> <a href="ViewNewServlet?idnews=${n.idnews}" title=${n.title}>${n.title}</a></li>
                        </c:forEach>
                    </ul>
                </div>
                <script>
                    _showDynClickBatch(['dynclicks_u9_5926', 'dynclicks_u9_5925', 'dynclicks_u9_5923', 'dynclicks_u9_5918', 'dynclicks_u9_5913', 'dynclicks_u9_5896', 'dynclicks_u9_5894', 'dynclicks_u9_5944'], [5926, 5925, 5923, 5918, 5913, 5896, 5894, 5944], "wbnews", 1459835785)
                </script>
            </div>
            <div class="vsb-space informBox informBox3 fl" columns="2">
                <div class="dynamic cleafix">
                    <h2>专题列表</h2>
                </div>
                <div class="infor_list">
                    <ul class="dynamic_list dynamic_list1">
                        <c:forEach var="n" items="${lsNews_by_5}" >
                            <li> <a href="ViewNewServlet?idnews=${n.idnews}" title=${n.title}>${n.title}</a></li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <!--广告-->
        <!--foot-->
        <div class="footWrap clearfix">
            <div class="footDiv_one">
                <div class="foot_one">
                    <p></p>
                    <div>Copyright© 2018 All Rights Reserved. 西南石油大学计算机科学学院</div>
                    <p></p>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {
                    $(".select").each(function() {
                        var s = $(this);
                        var z = parseInt(s.css("z-index"));
                        var dt = $(this).children("dt");
                        var dd = $(this).children("dd");
                        var _show = function() {
                            dd.slideDown(200);
                            dt.addClass("cur");
                            s.css("z-index", z + 1);
                        }; //展开效果
                        var _hide = function() {
                            dd.slideUp(200);
                            dt.removeClass("cur");
                            s.css("z-index", z);
                        }; //关闭效果
                        dt.click(function() {
                            dd.is(":hidden") ? _show() : _hide();
                        });
                        dd.find("a").click(function() {
                            dt.html($(this).html());
                            _hide();
                        }); //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
                        $("body").click(function(i) {
                            !$(i.target).parents(".select").first().is(s) ? _hide() : "";
                        });
                    })
                });
            </script>
            <script type="text/javascript">
                $(function() {
                    //切换对象为其他，这里为包含图片和标题的层
                    $('#change_32 div.changeDiv').soChange({ //对象指向层，层内包含图片及标题
                        thumbObj: '#change_32 .ul_change_a2 span',
                        thumbNowClass: 'on' //自定义导航对象当前class为on
                    });
                });
            </script>
            <script type="text/javascript">
                function setContentTab(name, curr, n) {
                    for (i = 1; i <= n; i++) {
                        var menu = document.getElementById(name + i);
                        var cont = document.getElementById("con_" + name + "_" + i);
                        if (menu != null) {
                            menu.className = i == curr ? "up" : "";
                        }
                        if (i == curr) {
                            if (cont != null) {
                                cont.style.display = "block";
                            }
                        } else {
                            if (cont != null) {
                                cont.style.display = "none";
                            }
                        }
                    }
                }
            </script>
            <script type="text/javascript">
                $(".pic ul li").hover(function() {
                    $(this).stop(true).animate({
                        width: "848px"
                    }, 500).siblings().stop(true).animate({
                        width: "50px"
                    }, 500);
                });
            </script>
        </div>
    </div>
</body>
</html>